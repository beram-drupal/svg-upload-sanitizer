<?php

namespace Drupal\Tests\svg_upload_sanitizer\Unit\HookHandler;

use Drupal\svg_upload_sanitizer\Helper\FileHelper;
use Drupal\svg_upload_sanitizer\Helper\SanitizerHelper;
use Drupal\svg_upload_sanitizer\HookHandler\FileInsertHookHandler;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Unit test class for the FileInsertHookHandler class.
 *
 * @package Drupal\Tests\svg_upload_sanitizer\Unit\HookHandler
 *
 * @internal
 */
class FileInsertHookHandlerTest extends TestCase {

  public function testCreate(): void {
    $container = $this->createMock(ContainerInterface::class);
    $container
      ->expects($this->atLeast(2))
      ->method('get')
      ->willReturnCallback(function (string $id) {

        if ($id === 'svg_upload_sanitizer.helper.sanitizer') {
          return $this->createMock(SanitizerHelper::class);
        }

        if ($id === 'svg_upload_sanitizer.helper.file') {
          return $this->createMock(FileHelper::class);
        }

        return null;
      });

    $this->assertInstanceOf(FileInsertHookHandler::class, FileInsertHookHandler::create($container));
  }

}
